##################################################
# DNA evolved
# K Akram a small change
# Feb - April 2021
#
# original code by:
# DNA evolution simulations
# Dr Andrew Dalby
# 20/01/2021
#
###################################################
library("seqinr")
#setwd('/Users/khalid/Documents/2ndYear/Bioinformatics/project/newCode')

#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
filename <- ""
mutationsPer <- 100
numRows <- 40

# test if there is at least one argument: if not, return an error
if (length(args)==0 || length(args) < 3) {
	stop("At least three arguments must be supplied (input file and mutationsPer and numRows).\n", call.=FALSE)
} else {
  	filename <- args[1]
	mutationsPer <- strtoi(args[2])
	numRows <- strtoi(args[3])
}

mystr <- read.fasta(file = "hema.fasta", as.string = TRUE)[[1]]
mystr <- toupper(mystr)

seq <- strsplit(mystr,"")[[1]]

# settings
numElements <- nchar(mystr) # how many nucleotides in one row
numMutations <- round(numElements / mutationsPer)
totalElements <- numRows*numElements
mutType <- 2 # 1 is Jukes-Cantor, 2 is Kimura 80, 3 is twelveParam
# read in the exclusions array 
protect<-read.table("protect.txt",header=TRUE,sep=" ")

print(protect)

#seq1 <- sample(c("A","C","T","G"),126, replace=TRUE)
corr.matrix <- matrix(1:totalElements, nrow=numRows, ncol=numElements)


getElementToModify <- function() {
	retry <- TRUE;
	while (retry){
  		i <- sample(1:numElements,1) # get a nucleotide to modify

		retry <- FALSE
		j <- 1
		while(j < nrow(protect)){
			if (i >= protect[j,1] && i <= protect[j,2]){
				retry <- TRUE
			}
			j <- j+1
		}
	}
	
	return(i)
}

jukesCantor <- function(){
  	j <- getElementToModify()
 	m <- sample(c("A","C","G","T"),1)
  	seq[j] <- m
	return(seq)
}

# implementation of K80 - two parameters - alpha and beta
# a very simple implementation, assuming 2x more transitions
# (A<->G) and (C<->T), than (A<->C, A<->T, G<->C, G<->T)
kimura <- function(){
  	j <- getElementToModify()

	if (seq[j] == 'A' || seq[j] == 'G'){
		m <- sample(c("A","G","C","T"),1,replace=TRUE,prob=c(.35,.35,.15,.15)) # transition 2x > transversion
	}
	if (seq[j] == 'C' || seq[j] == 'T'){
		m <- sample(c("C","T","A","G"),1,replace=TRUE,prob=c(.35,.35,.15,.15)) # transition 2x > transversion
	}
  	seq[j] <- m
	return(seq)

}

# twelveParam - independently assign probabilities to each mutation possibility
twelveParam <- function() {
  	j <- getElementToModify()

	if (seq[j] == 'A'){
		m <- sample(c("A","G","C","T"),1,replace=TRUE,prob=c(.25,.15,.15,.45))  
	}
	if (seq[j] == 'G'){
		m <- sample(c("A","G","C","T"),1,replace=TRUE,prob=c(.15,.25,.15,.45))  
	}
	if (seq[j] == 'C'){
		m <- sample(c("A","G","C","T"),1,replace=TRUE,prob=c(.15,.15,.25,.45))  
	}
	if (seq[j] == 'T'){
		m <- sample(c("A","G","C","T"),1,replace=TRUE,prob=c(.30,.30,.15,.25))  
	}

  	seq[j] <- m
	return(seq)
}


print("Here 1")

for (i in 1:numRows){
	for (j in 1:numMutations){
	  	if (mutType == 1) {
			seq = jukesCantor()
	  	} else if ( mutType == 2){
			seq = kimura()
	  	}
	}
  #print(seq)
  corr.matrix[i,] <- seq 
}


# collpase the Matrix ... :-)
final <- '' 
for (r in 1:nrow(corr.matrix)){
	final = paste(final,'\n> Nucleotides\n', sep="", collapse="")
    for (c in 1:ncol(corr.matrix)){
		 final = paste(final, corr.matrix[r,c], sep="", collapse="")	
	}
}

time <- Sys.time()
opfilename <- paste("output_", args[2], "_", args[3], "_", time, ".fasta", sep="", collapse="")
fileConn<-file(opfilename)
writeLines(c(final), fileConn)
close(fileConn)

# write data out to a file
#mystr = apply(corr.matrix, 1, paste, collapse = "")
#write.table(corr.matrix, file="mymatrix.txt", row.names=FALSE, col.names=FALSE)
#write.table(mystr, file="mymatrix.txt", row.names=FALSE, col.names=FALSE)

print("End")


