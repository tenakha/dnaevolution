This repo contains the code for simulating DNA mutations in R.
It is designed to be executed on the command line thus:

Rscript DNA.R
